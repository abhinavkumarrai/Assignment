const inventory = require('./inventory.js');
const getLastCarInInventory = require('./problem2');


const lastCar = getLastCarInInventory(inventory);


if(!Array.isArray(lastCar)) {
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`)
} else {
    console.log(lastCar);
}

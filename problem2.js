function getLastCarInInventory(inventory) {
    if (Array.isArray(inventory) && inventory.length > 0) {
        let max = -1;
        let ind = -1;
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].id > max) {
                max = inventory[i].id;
                ind = i;
            }
        }
        if (max !== -1){
            return inventory[ind];
        } else {
            return [];
        }
    } else {
        return [];
    }
}

module.exports = getLastCarInInventory;

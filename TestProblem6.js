const inventory = require('./inventory');
const BMWAndAudi = require('./problem6');

const preferredCarsList = BMWAndAudi(inventory, 'land rover') // we can pass second argument as car_maker name or an array of car_maker names.

if (Array.isArray(preferredCarsList) && preferredCarsList.length > 0) {
    console.log(JSON.stringify(preferredCarsList));
} else {
    console.log(preferredCarsList);

}

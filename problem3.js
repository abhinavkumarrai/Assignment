function sortCarModels(inventory) {
    if (Array.isArray(inventory) && inventory.length > 0) {
        let sortedInventory = inventory.sort((a, b)=>{
            if(a.car_model.toUpperCase() > b.car_model.toUpperCase()) {
                return 1;
            } else {
                return -1;
            }
        })
        return sortedInventory;
    } else {
        return [];
    }    
}

module.exports = sortCarModels;

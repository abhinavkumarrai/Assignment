function BMWAndAudi(inventory, preferredCars) {
    
    if (Array.isArray(inventory) && inventory.length > 0 && preferredCars && preferredCars.length > 0){    
        let BMWAndAudi = [];

        if (Array.isArray(preferredCars)) {
            for(let i = 0; i < preferredCars.length; i++) {
                preferredCars.push(preferredCars.shift().toString().toLowerCase());
            }
        } else if (typeof preferredCars === 'string') {
            preferredCars = [preferredCars.toLowerCase()];
        }
    
        for (let i = 0; i < inventory.length; i++) {
            if (preferredCars.includes(inventory[i].car_make.toLowerCase())) {
                BMWAndAudi.push(inventory[i]);
            }
        }
    
        return BMWAndAudi;
    } else {
        return [];
    }
}

module.exports = BMWAndAudi;

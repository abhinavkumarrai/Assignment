function getYearsOfCars(inventory) {
    
    if (Array.isArray(inventory) && inventory.length > 0) {
        let yearsArray = [];

        if (inventory[0].id === undefined){
            return [];
        }

        for (let i = 0; i < inventory.length; i++) {
            yearsArray.push(inventory[i].car_year);
        } 

        return yearsArray;
        
    } else {
        return [];
    }
}

module.exports = getYearsOfCars;

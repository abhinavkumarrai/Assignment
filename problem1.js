function getCarById(inventory, id) {

    if (Array.isArray(inventory) && inventory.length > 0) {
        for(let i = 0; i < inventory.length; i++) {
            if(inventory[i].id === id) {
                return inventory[i];
            }
        }
        return [];
    } else {
        return [];
    }
}

module.exports = getCarById;

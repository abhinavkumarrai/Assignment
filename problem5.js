function getYearsOfCarsBefore2000(inventory) {
    
    if (Array.isArray(inventory) && inventory.length > 0) {
        let yearsArray = [];
        
        for (let i = 0; i < inventory.length; i++) {
            if(inventory[i] < 2000){
                yearsArray.push(inventory[i]);
            }
        }
        return yearsArray;
    } else {
        return [];
    }
}

module.exports = getYearsOfCarsBefore2000;

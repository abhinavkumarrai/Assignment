const inventory = require('./inventory.js');
const getCarById = require('./problem1.js');

const car = getCarById(inventory, 33);
if(Array.isArray(car) || car === -1) {
    console.log(car);
}else {
    console.log(`Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`)  
}
